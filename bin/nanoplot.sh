#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Visualizations of sequencing data using Nanoplot         ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPU=${args[0]}
FASTQ_FILE=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="NanoPlot -t ${NCPU} --plots kde dot hex --N50 --fastq  ${FASTQ_FILE} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}