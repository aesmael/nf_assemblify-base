process hifiasm {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    tag "hifiasm"

    // Result files published in Netxtflow result directory.
    publishDir "${params.resultdir}/02a_hifiasm",	mode: 'copy', pattern: '*.bp.p_ctg.fa'
    publishDir "${params.resultdir}/logs/hifiasm",	mode: 'copy', pattern: 'hifi*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'hifi*.cmd'

    // Workflow input stream.
    input:
        path(qc_ch)

    // Workflow output stream.
    output:
        path("*.bp.p_ctg.fa"), emit: assembly_fa
        path("hifi*.log")
        path("hifi*.cmd")

    // Script to execute
    script:
    """
    hifiasm.sh $qc_ch ${task.cpus} hifiasm.cmd >& hifiasm.log 2>&1
    """ 
}



