process nanoplot {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'lowmen'

    // tag is used to display task name in Nextflow logs
    tag "nanoplot"

    // Result files published in Netxtflow result directory.
    publishDir "${params.resultdir}/02b_nanoplot",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/02b_nanoplot",	mode: 'copy', pattern: '*.png'
    publishDir "${params.resultdir}/02b_nanoplot",	mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/logs/nanoplot",	mode: 'copy', pattern: 'nano*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'nano*.cmd'

    // Workflow input stream.
    input:
        path(qc_ch)

    // Workflow output stream.
    output:
        path("*.html")
        path("*.png")
        path("nano*.log")
        path("*.txt")
        path("nano*.cmd")

    // Script to execute
    script:
    """
    nanoplot.sh ${task.cpus} $qc_ch nanoplot.cmd >& nanoplot.log 2>&1
    """ 
}
