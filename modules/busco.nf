process busco {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    tag "busco"

    // Result files published in Netxtflow result directory.
    publishDir "${params.resultdir}/02b_busco",		mode: 'copy', pattern: '*.busco'
    publishDir "${params.resultdir}/logs/busco",	mode: 'copy', pattern: 'busco*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'busco*.cmd'

    // Workflow input stream.
    input:
        path(budsco_db_path)
	    val(busco_db_name)
	    path(assembly_fa)

    // Workflow output stream.
    output:
        path("*.busco")
        path("busco*.log")
        path("busco*.cmd")

    // Script to execute
    script:
    """
    busco.sh ${task.cpus} ${budsco_db_path} ${busco_db_name} ${assembly_fa} busco.cmd  >& busco.log 2>&1
    """ 
}
