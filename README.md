#  **Assemblify-base pipeline : FAIR principles applied to genome assembly and annotation**

This is a project associated to this [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).


## Introduction

The main aim of this project is to develop Assemblify-base, a [Nextflow](https://www.nextflow.io/) pipeline (version 23.04.1) for genome assembly and annotation, in particular that of the *Dunaliella primolecta* species. We have also focused on creating a pipeline that respects [FAIR principles](https://www.go-fair.org/fair-principles/), to make our project Findable, Accessible, Interperable, and Reusable. 

The Nextflow pipeline is an automated pipeline that allows users to easily launch the various assembly and annotation stages using a command line. 

## Biological data :

The genome used is that of *Dunaliella primolecta*, a single-cell species of microalga belonging to the Cholophyceae class.
Two types of sequencer-generated data are available for genome assembly: 
 - Illumina sequencer: RNA sequences 
 - PacBio sequencer: HIFI

Data are available on the European Nucleotide Archive ENA website: 
- Sample Accesions : 
    - RNA PolyA : [SAMEA7524530](https://www.ebi.ac.uk/biosamples/samples/SAMEA7524530)
    - PacBio HIFI: [SAMEA8100059](https://www.ebi.ac.uk/biosamples/samples/SAMEA8100059)
- Run Accessions :
    - RNA PolyA : [ERR6688549](https://www.ebi.ac.uk/ena/browser/view/ERR6688549)
    - PacBio HIFI: [ERR6939244](https://www.ebi.ac.uk/ena/browser/view/ERR6939244)

All genome data can be found on the ENA website, under accession number [PRJEB46283](https://www.ebi.ac.uk/ena/browser/view/PRJEB46283).

## Computing requirement: 

This pipeline has been developed on the IFB cluster, using On-Demand cluster, with the following parameters :
- OS : Unix
- Partition: fast
- Number of CPUs: 2
- Amount of memory: 8G

### Installation 
Before using the pipeline, the user must ensure that : 
- install [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) 
- install [Singularity](https://docs.sylabs.io/guides/3.0/user-guide/)

To fork the project, users must log into their personal gitlab space, go to [nf_assemblify-base](https://gitlab.com/ifb-elixirfr/training/workflows/nf_assemblify-base) on Git lab, and click on <b>fork</b>. 
The user can then create a git repository and clone the fork link. 
```bash
mkdir assemblify-tmp
git clone https://gitlab.com/<username>/nf_assemblify-base.git
```

### Containers 
To build the pipeline, we used the following containers, also available on the [Biocontainers](https://biocontainers.pro/registry) website :
- **fastqc :** quay.io/biocontainers/fastqc:0.11.9--hdfd78af_1
- **multiqc :** quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0
- **nanoplot :** quay.io/biocontainers/nanoplot:1.32.1--py_0
- **hifiasm :** quay.io/biocontainers/hifiasm:0.18.9--h5b5514e_0
- **busco :** quay.io/biocontainers/busco:5.5.0--pyhdfd78af_0
- **Red :** quay.io/biocontainers/red:2018.09.10--h4ac6f70_2
- **Hisat2 :** quay.io/biocontainers/hisat2:2.2.1--hdbdd923_6
- **samtools :** quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0

## Pipeline Overview : 

![The pipeline diagram](pipeline-gaa.png) 


Obtained on the [practical course](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/). 

# Pipeline Project : 

To launch a job using SLURM : 
```bash
sbatch run_assemblify.slurm 
```

To monitor the progress of your job : 
```bash
squeue -u $USER
```
Replace $USER with your username


## Stage 1 - Quality Control (QC)

For the Illumina RNA-seq : 
### **FastQC** 
Completed

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (version : 0.11.9) : Quality control for long-read sequencing data. 
- Input data : Illumina sequencing files (.fastq.gz)
- Output data : Web report (.html), plots (.png), and statistics (.txt).

Script : ./nf_assemblify-base/bin/nanoplot.sh

### **MultiQC** 
Completed

[MultiQC](https://github.com/MultiQC/MultiQC) (version : 1.14) : Quality control including analysis into a single report. 
- Input data  : Archive from FastQC: (.zip).
- Output data : Web report (.html).

Script: ./nf_assemblify-base/bin/fastqc.sh


For the PacBio sequences : 

### **Nanoplot** 
Completed

[Nanoplot](https://biocontainer-doc.readthedocs.io/en/latest/source/nanoplot/nanoplot.html) (version 1.32.1) : Plotting tool for long-read sequencing data.
- Input data : PacBio sequencing files (.fastq.gz).
- Output data : Web report (.html), plots (.png), and statistics (.txt).

Script: ./nf_assemblify-base/bin/nanoplot.sh

## Stage 2 - Contiging :

### **Hifiasm** 
Completed

[Hifiasm](https://hifiasm.readthedocs.io/en/latest/) (version 0.18.9) : Uses for haplotype-resolved de novo assembly designed for PacBio HiFi reads.
- Input data : PacBio sequencing files (.fastq.gz).
- Output data : Assembly sequences (.fasta), Assembly graph (.gfa).

Script: ./nf_assemblify-base/bin/hifiasm.sh

### **Busco** 
This step is completed but **optional**, if you wish to use it, go to the main.nf file and make this part executable. 

[Busco](https://anaconda.org/bioconda/BUSCO) (version 5.5.0) : Provides measures for quantitative assessment of genome.
- Input data : Assembly sequences from Hifiasm (.fasta).
- Output data : Statistics (.tsv), Subfolders.

Script: ./nf_assemblify-base/bin/busco.sh

## Stage 3 - Repeat Masking

### **RED** 
Completed but not merged, wainting to be tested. 

[RED](https://github.com/nextgenusfs/redmask/tree/master?tab=readme-ov-file) (version 2018.09.10) : Identifies and masks repeats for structural annotation.
- Input data : Assembly sequences from Hifiasm (.fasta).
- Output data : Softmasked assembly sequences (.msk).

Script: ./nf_assemblify-base/bin/red.sh

## Stage 4 - Evidences

### **Hisat2** 
Under Developement

[Hisat2](http://daehwankimlab.github.io/hisat2/) (version 2.2.1) :  For aligning sequencing reads to a reference genome.
- Input data : Illumina sequencing files (.fastq.gz), Softmasked assembly sequences from RED (.msk).
- Output data : Genome index (.ht2), Mapping file (.bam).

Script: ./nf_assemblify-base/bin/hisat2.sh

### **Samtools** 
Under Developement

[Samtools](https://anaconda.org/bioconda/Samtools) (version 1.19.2) : For interacting with high-throughput sequencing data.
- Input data : Mapping file (.bam).
- Output data : Mapping index file (.bai).

Script: ./nf_assemblify-base/bin/samtools.sh



## Authors 
- [Youness ELAMMANY](https://gitlab.com/Youness44), Master 2 student in Bioinformatics at University of Rennes - youness.el-ammany@etudiant.univ-rennes.fr
- [Asraa ESMAEL](https://gitlab.com/aesmael), Master 2 student in Bioinformatics at University of Rennes - asraa.esmael@etudiant.univ-rennes.fr


## Acknowledgement
This project was part of the "FAIR principles applied to genome assembly and annotation" module.
You can find the project guidelines on the following [link](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/).


## Feedback
**This project is still under development**, we are working on automating it, as well as adding the prediction part. 

We welcome your feedback to help us improve this pipeline and make it more efficient. 
If you encounter any problems, have any questions or suggestions, please do not hesitate to contact us at the following e-mail address: 
- Youness ELAMMANY : youness.el-ammany@etudiant.univ-rennes.fr
- Asraa ESMAEL : asraa.esmael@etudiant.univ-rennes.fr

## Licence 
This work is licensed under Attribution-ShareAlike 4.0 International.

This license requires that reusers give credit to the creator. It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, even for commercial purposes. If others remix, adapt, or build upon the material, they must license the modified material under identical terms.
